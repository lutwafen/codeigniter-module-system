$(document).ready(function() {
  var component_id = 1;

  $(document).delegate(".selected_property", "change", function() {
    var selected_value = $(this).val();
    $.ajax({
      url: "http://localhost/codeigniter-module-system/Page/PropertyDetail",
      type: "post",
      data: { selected: selected_value },
      success: function(view) {
        $(".property-description").html(view);
      },
      error: function(err) {
        console.log(err);
        alert("Özellik detaylarında bir hata var!");
      }
    });
  });

  $(document).delegate("#use_select_component", "change", function() {
    if ($(this).val().length > 0) {
      $.ajax({
        url: "http://localhost/codeigniter-module-system/Page/ComponentDetail",
        type: "post",
        data: { selected_components: $(this).val() },
        success: view => {
          $(".selected-component-property").html(view);
          $(".btnCreatePage").show("slow");
        },
        error: err => {
          alert("Hata var");
          console.log("Bileşen detayları alınırken bir hata oluştu!");
        }
      });
    } else {
      $(".btnCreatePage").hide("slow");
    }
  });
});
