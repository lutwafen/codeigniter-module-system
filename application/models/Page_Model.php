<?php
class Page_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ListPages()
    {
        $this->db->select("*")->from("page");
        if (!$this->session->UserAdmin) {
            $this->db->join("pageaccess pa", "pa.PageID = page.PageID and pa.UserID = " . $this->session->UserID);
        }

        return $this->db->get()->result();
    }

    public function ListProperty()
    {
        $this->db->select("PropertyName, PropertyID");
        $this->db->from("property");
        $result = $this->db->get()->result();
        return $result;
    }

    public function PropertyDescription($property_id)
    {
        $this->db->select("PropertyDescription");
        $this->db->from("property");
        $this->db->where("PropertyID", $property_id);
        return $this->db->get()->result();
    }

    public function CreateComponent($component_name)
    {
        $this->db->insert("component", array("ComponentName" => $component_name));
        return $this->db->insert_id();
    }

    public function componentAddProperty($component_id, $property_id)
    {
        $this->db->insert("componentproperty", array("ComponentID" => $component_id, "PropertyID" => $property_id));
    }

    public function listComponents()
    {
        $this->db->select("*");
        $this->db->from("component");
        return $this->db->get()->result();
    }

    public function componentDetail($component_id)
    {
        $this->db->select("p.PropertyDescription");
        $this->db->from("componentproperty cp");
        $this->db->join("property p", "p.PropertyID = cp.PropertyID");
        $this->db->where("cp.ComponentID", $component_id);
        return $this->db->get()->result();
    }
    public function CreatePage($page_name)
    {
        $this->db->insert("page", array("PageName" => $page_name));
        return $this->db->insert_id();
    }

    public function CreatePageComponent($page_id, $component_id)
    {
        $this->db->insert("pagecomponent", array("PageID" => $page_id, "ComponentID" => $component_id));
    }

    public function showPage($page_id)
    {
        $this->db->select("*");
        $this->db->from("vw_show_page");
        $this->db->where("PageID", $page_id);
        if (!$this->session->UserAdmin) {
            $this->db->where("UserID", $this->session->UserID);
        }
        return $this->db->get()->result();
    }

    public function UserPageAccess($user_id, $page_id)
    {
        $this->db->insert("pageaccess", array("UserID" => $user_id, "PageID" => $page_id));
    }

}
