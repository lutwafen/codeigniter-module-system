<?php
class Page extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ListProperty()
    {
        $this->db->select("PropertyName, PropertyID");
        $this->db->from("property");
        $result = $this->db->get()->result();
        return $result;
    }

    public function PropertyDescription($property_id)
    {
        $this->db->select("PropertyDescription");
        $this->db->from("property");
        $this->db->where("PropertyID", $property_id);
        return $this->db->get()->result();
    }
}
