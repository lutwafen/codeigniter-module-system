<?php
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->UserLogin) {
            redirect(base_url("Panel"));
        }
    }
    public function index()
    {

        $viewData = new stdClass();

        if ($this->input->post('btnLogin') && $this->input->post('username') && $this->input->post('password')) {
            $this->load->model("Users");
            $result = $this->Users->login($this->input->post("username"), $this->input->post("password"));

            if (isset($result->id) && $result->id > 0) {
                $this->session->UserLogin = true;
                $this->session->UserID    = $result->id;
                $this->session->UserData  = $result;
                $this->session->UserAdmin = $result->is_admin == 1 ? true : false;
                redirect("/");
            } else {
                $this->session->set_flashdata("error", "Kullanıcı adı veya parola hatalı!");
            }

        } else {
            $this->session->set_flashdata("error", "Post Edilemedi!");
        }

        $this->load->view("Page/Login", $viewData);
    }
}
