<?php
class Page extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function PropertyDetail()
    {
        if (isset($_POST)) {
            $this->load->model("Page_Model");
            echo '<ul>';
            foreach ($this->input->post('selected') as $selected_property) {
                //echo $selected_property.'<br> <hr>';
                $result = $this->Page_Model->PropertyDescription($selected_property);
                echo '<li>' . $result[0]->PropertyDescription . '</li>';
            }
            echo '</ul>';
        }
    }
    public function ComponentDetail()
    {
        if (isset($_POST['selected_components'])) {
            $this->load->model("Page_Model");
            echo '<ul>';
            foreach ($this->input->post('selected_components') as $component_id) {
                $result = $this->Page_Model->componentDetail($component_id);
                foreach ($result as $row) {
                    echo '<li>' . $row->PropertyDescription . '</li>';
                }
            }
            echo '</ul>';
        }
    }
}
