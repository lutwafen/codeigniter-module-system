<?php
class Panel extends CI_Controller
{
    private $pages;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->UserLogin) {
            redirect(base_url("Login"));
        }

        $this->load->model("Page_Model");
        $this->pages           = new stdClass();
        $this->pages->pageList = $this->Page_Model->ListPages();
    }

    public function index()
    {
        $this->load->view("Inc/header", $this->pages);
        $this->load->view("Page/PanelV");
        $this->load->view("Inc/footer");
    }

    public function ShowPage($page_name = "", $page_id = 0)
    {
        if ($page_id > 0 && $page_name != "") {
            $this->load->view("Inc/header", $this->pages);
            $showData       = new stdClass();
            $showData->Data = $this->Page_Model->showPage($page_id);
            $this->load->view("Page/ShowPage", $showData);
            $this->load->view("Inc/footer");
        } else {
            redirect(base_url("Panel"));
        }
    }

    public function CreatePage()
    {
        $this->load->view("Inc/header", $this->pages);
        $data                = new stdClass();
        $data->ComponentList = $this->Page_Model->listComponents();
        $data->UserList      = $this->db->select("*")->from("users")->get()->result();
        if (isset($_POST['create_page'])) {
            //print_r($_POST);
            $page_id = $this->Page_Model->CreatePage($this->input->post('page_name'));
            foreach ($this->input->post('use_components') as $component) {
                $this->Page_Model->CreatePageComponent($page_id, $component);
            }

            foreach ($this->input->post("access_users") as $access_user) {
                $this->Page_Model->UserPageAccess($access_user, $page_id);
            }
        }

        $this->load->view("Page/CreatePage", $data);
        $this->load->view("Inc/footer");
    }

    public function CreateComponent()
    {
        $this->load->view("Inc/header", $this->pages);
        $data                = new stdClass();
        $data->property_data = $this->Page_Model->ListProperty();
        //print_r($property_data);

        if (isset($_POST['createComponentButton'])) {
            $last_id = $this->Page_Model->CreateComponent($this->input->post('component_name'));
            foreach ($this->input->post("selected_property") as $property) {
                // echo 'Selected Property ID: ' . $property . '<br>';
                $this->Page_Model->componentAddProperty($last_id, $property);
            }
        }

        $this->load->view("Page/CreateComponent", $data);
        $this->load->view("Inc/footer");
    }

    public function Logout()
    {
        $this->session->sess_destroy();
        redirect("/Login");
    }
}
