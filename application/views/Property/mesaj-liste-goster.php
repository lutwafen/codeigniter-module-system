<div class="row">
    <div class="col-lg-12">
        <h3>Mesajların Gösterilmesi</h3>
        <hr>
    </div>
    <div class="col-lg-12">
        <table border="1" style="width:100%;">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Mesaj Konusu</td>
                    <td>Mesajı Gönderen</td>
                    <td>
                        İşlem
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Yeni bir mesaj konusu</td>
                    <td>Fatih Çalışan</td>
                    <td>
                        <a href="">Yanıtla</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
