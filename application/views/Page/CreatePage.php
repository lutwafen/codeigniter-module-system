
<div class="col-lg-9">
    <h3>Sayfa Oluştur</h3>
    <hr>
    <form action="" method="post" class="row">
        <div class="col-lg-12">
            Sayfa Adı
            <input type="text" name="page_name" style="width:100%;">
        </div>

        <div class="col-lg-12">
            Kullanılacak Bileşenleri Seç
            <select name="use_components[]" multiple style="width:100%;" id="use_select_component">
            <?php foreach ($ComponentList as $component): ?>
                <option value="<?=$component->ComponentID?>"><?=$component->ComponentName?></option>
            <?php endforeach;?>
            </select>
        </div>

        <div class="col-lg-12">
                Bu Sayfaya Erişim İzni Olan Personelleri Seçin
                <select name="access_users[]" multiple style="width:100%;" id="">
                    <?php foreach ($UserList as $user): ?>
                    <option value="<?=$user->id?>">@<?=$user->username?> - <?=$user->name?></option>
                    <?php endforeach;?>
                </select>
        </div>

        <div class="col-lg-12 selected-component-property"></div>

        <button name="create_page" class="btnCreatePage" style="width:100%; display:none;">Sayfayı Oluştur</button>
    </form>
</div>
