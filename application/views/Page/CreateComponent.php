<form action="" method="post" class="col-lg-9">

    <h3>Bileşen Oluştur</h3>
    <hr>
    <div class="row">

        <div class="col-lg-6">Bileşen Adını Giriniz</div>
        <div class="col-lg-6">
            <input type="text" name="component_name" style="width:100%;" id="">
        </div>


        <div class="col-lg-6">
            <strong> Özellikleri Seç </strong>
        </div>
        <div class="col-lg-6">
            <select class="selected_property" name="selected_property[]" style="width:100%;" multiple>
                <?php foreach ($property_data as $property): ?>
                <option value="<?=$property->PropertyID?>"><?=$property->PropertyName?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="property-description">
        <hr>
        Seçilen Özelliklerin Açıklaması
    </div>

    <button type="submit" name="createComponentButton" value="1">Bileşeni Oluştur</button>
</form>