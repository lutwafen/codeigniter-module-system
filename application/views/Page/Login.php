<html>
	<head>
		<title> Giriş Yap </title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css" type="text/css" >

	</head>
	<body>
		<style type="text/css">
			.box{
				font-family: Arial;
				background-color:#ddD;
				border:1px solid red;
				margin:10px auto;
				border-radius: 7px;
				padding:7px 7px;
			}
		</style>
		<?php if ($this->session->flashdata("error")) {?>
		<div class="row box">
			<div class="col-lg-12">
				<h3> Hata Var</h3>
			</div>
			<div class="col-lg-12">
				<p><?=$this->session->flashdata("error");?></p>
			</div>
		</div>
		<?php }?>
		<form action="" method="post" class="box row">
			<div class="form-element col-lg-4 row">
				<label class="col-lg-3"> Kullanıcı Adı </label>
				<input type="text" class=" col-lg-8" name="username">
			</div>
			<div class="form-element col-lg-4 row">
				<label class="col-lg-3"> Parola </label>
				<input type="password" class=" col-lg-8" name="password">
			</div>
			<div class="form-element col-lg-4 row">
				<button type="submit" class="col-lg-12" name="btnLogin" value="1">Giriş Yap</button>
			</div>
		</form>

	</body>
</html>