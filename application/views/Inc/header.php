<head>
	<title><?=isset($title) ? $title : "dashboard"?></title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css" type="text/css" >
<script src="https://code.jquery.com/jquery-3.4.1.min.js"> </script>

</head>
<div class="row">
    <div class="col-lg-3">
        <ul style="list-style:none;">
            <li>
                <h4>Yönlendirme</h4>
            </li>
            <?php if ($this->session->UserAdmin == true): ?>
                <li>
                    <a href="<?=base_url('Panel/')?>"> Panel </a>
                </li>
                <li>
                    <a href="<?=base_url('Panel/CreatePage')?>"> Sayfa Oluştur</a>
                </li>
                <li>
                    <a href="<?=base_url('Panel/CreateComponent')?>"> Bileşen Oluştur</a>
                </li>
            <?php endif;?>
            <li>
                <a href="<?=base_url('Panel/Logout')?>"> Çıkış Yap</a>
            </li>
            <li>
                <hr>
                <h4>Sayfalar</h4>
                <small style="color:red;">
                    * Yönetici Olduğunuz İçin Tüm Sayfaları Görme Yetkisine Sahipsiniz!
                </small>
                <br>
                <br>
            </li>
            <?php foreach ($pageList as $p): ?>
            <li>
                <a href="<?=base_url('Panel/ShowPage/' . $p->PageName . '/' . $p->PageID)?>"><?=$p->PageName?></a>
            </li>
            <?php endforeach;?>
        </ul>
    </div>