-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 20 Oca 2020, 12:52:51
-- Sunucu sürümü: 10.4.8-MariaDB
-- PHP Sürümü: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `ci-module-system`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `component`
--

CREATE TABLE `component` (
  `ComponentID` int(11) NOT NULL,
  `ComponentName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `component`
--

INSERT INTO `component` (`ComponentID`, `ComponentName`) VALUES
(7, 'test'),
(10, 'test 2 bileşeni'),
(11, 'Bir bileşen oluştur');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `componentproperty`
--

CREATE TABLE `componentproperty` (
  `ComponentPropertyID` int(11) NOT NULL,
  `ComponentID` int(11) DEFAULT NULL,
  `PropertyID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `componentproperty`
--

INSERT INTO `componentproperty` (`ComponentPropertyID`, `ComponentID`, `PropertyID`) VALUES
(1, 10, 1),
(2, 10, 2),
(3, 11, 3),
(4, 7, 2);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `page`
--

CREATE TABLE `page` (
  `PageID` int(11) NOT NULL,
  `PageName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `page`
--

INSERT INTO `page` (`PageID`, `PageName`) VALUES
(2, 'Gelen Mesajlar'),
(3, 'Siparişler'),
(4, 'Sipariş Listesini Gör');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pageaccess`
--

CREATE TABLE `pageaccess` (
  `ID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `pageaccess`
--

INSERT INTO `pageaccess` (`ID`, `UserID`, `PageID`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 4);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pagecomponent`
--

CREATE TABLE `pagecomponent` (
  `PageComponentID` int(11) NOT NULL,
  `PageID` int(11) DEFAULT NULL,
  `ComponentID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `pagecomponent`
--

INSERT INTO `pagecomponent` (`PageComponentID`, `PageID`, `ComponentID`) VALUES
(1, 2, 10),
(3, 3, 11),
(4, 4, 11);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `property`
--

CREATE TABLE `property` (
  `PropertyID` int(11) NOT NULL,
  `PropertyName` varchar(255) DEFAULT NULL,
  `PropertyCode` varchar(255) DEFAULT NULL,
  `PropertyDescription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `property`
--

INSERT INTO `property` (`PropertyID`, `PropertyName`, `PropertyCode`, `PropertyDescription`) VALUES
(1, 'Mesaj Listesi', 'mesaj-liste-goster', 'Gelen Mesajların Gösterilmesini Sağlar'),
(2, 'Mesaj Yanıtla', 'gelen-mesaj-yanitla', 'Gelen Mesajlarının Seçilip, Yanıtlanmasını Sağlar'),
(3, 'Siparişler', 'gelen-siparis-listesi', 'Siparişlerin Listesini Gösterir'),
(4, 'Sipariş Onayı', 'gelen-siparis-onay', 'Gelen Siparişleri Onaylamak İçin Kullanılır');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `is_admin`) VALUES
(1, 'fatih', '1234', 'Fatih', 1),
(2, 'testuser', '1234', 'Test Kullanıcı', 0);

-- --------------------------------------------------------

--
-- Görünüm yapısı durumu `vw_show_page`
-- (Asıl görünüm için aşağıya bakın)
--
CREATE TABLE `vw_show_page` (
`PageID` int(11)
,`PageName` varchar(255)
,`PropertyCode` varchar(255)
,`ComponentName` varchar(255)
,`UserID` int(11)
);

-- --------------------------------------------------------

--
-- Görünüm yapısı `vw_show_page`
--
DROP TABLE IF EXISTS `vw_show_page`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_show_page`  AS  select `p`.`PageID` AS `PageID`,`p`.`PageName` AS `PageName`,`prop`.`PropertyCode` AS `PropertyCode`,`c`.`ComponentName` AS `ComponentName`,`pa`.`UserID` AS `UserID` from (((((`page` `p` left join `pagecomponent` `pg` on(`pg`.`PageID` = `p`.`PageID`)) left join `component` `c` on(`c`.`ComponentID` = `pg`.`ComponentID`)) left join `componentproperty` `cp` on(`cp`.`ComponentID` = `c`.`ComponentID`)) left join `property` `prop` on(`prop`.`PropertyID` = `cp`.`PropertyID`)) left join `pageaccess` `pa` on(`pa`.`PageID` = `p`.`PageID`)) ;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `component`
--
ALTER TABLE `component`
  ADD PRIMARY KEY (`ComponentID`);

--
-- Tablo için indeksler `componentproperty`
--
ALTER TABLE `componentproperty`
  ADD PRIMARY KEY (`ComponentPropertyID`);

--
-- Tablo için indeksler `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`PageID`);

--
-- Tablo için indeksler `pageaccess`
--
ALTER TABLE `pageaccess`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `pagecomponent`
--
ALTER TABLE `pagecomponent`
  ADD PRIMARY KEY (`PageComponentID`);

--
-- Tablo için indeksler `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`PropertyID`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `component`
--
ALTER TABLE `component`
  MODIFY `ComponentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Tablo için AUTO_INCREMENT değeri `componentproperty`
--
ALTER TABLE `componentproperty`
  MODIFY `ComponentPropertyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `page`
--
ALTER TABLE `page`
  MODIFY `PageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `pageaccess`
--
ALTER TABLE `pageaccess`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `pagecomponent`
--
ALTER TABLE `pagecomponent`
  MODIFY `PageComponentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `property`
--
ALTER TABLE `property`
  MODIFY `PropertyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
